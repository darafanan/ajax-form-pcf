import Vue from "vue";

import App from "./App.vue";
import axios from "axios";
import config from "./config";

axios.defaults.baseURL = config.baseURL;
axios.defaults.timeout = config.timeout;

new Vue({
  el: "#app",
  render: (h) => h(App),
});
