export default {
  processErrorMessage(errorData) {
    var errorMsg = [];

    for (var field in errorData) {
      var fieldMsg = errorData[field];
      for (var msg in fieldMsg) {
        var finalMsg = this.capitalFirstLetter(fieldMsg[msg]);
        errorMsg.push(this.capitalFirstLetter(finalMsg));
      }
    }
    return errorMsg.join("\n ");
  },
  capitalFirstLetter(msg) {
    return msg.charAt(0).toUpperCase() + msg.slice(1);
  },
  getErrorMessage(err) {
    var httpStatus = require("http-status-codes");
    const status = err.response.status;
    const reason = httpStatus.getReasonPhrase(status);
    const msg = "Error " + status + " " + reason;

    return msg;
  },
}