from rest_framework import status, viewsets, mixins
from rest_framework.response import Response
from rest_framework.decorators import api_view

from customer_records.models import CustomerSubscriptionRecord
from customer_records.serializers import CustomerSubscriptionRecordSerializer


class SubscriptionRecordsViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    model = CustomerSubscriptionRecord
    serializer_class = CustomerSubscriptionRecordSerializer
    queryset = CustomerSubscriptionRecord.objects.order_by('-created_at')


@api_view(['GET'])
def subscription_choices(request):
    choices = dict(CustomerSubscriptionRecord.SUBSCRIPTION_TYPES)

    return Response(choices, status=status.HTTP_200_OK)
