from django.db import models


class CustomerSubscriptionRecord(models.Model):
    SUBSCRIPTION_TYPES = (("free", "Free"), ("plus", "Plus"), ("pro", "Pro"))

    name = models.CharField(max_length=32)
    email = models.EmailField(unique=True)
    subscription = models.CharField(max_length=8, choices=SUBSCRIPTION_TYPES)
    created_at = models.DateTimeField(auto_now_add=True)
