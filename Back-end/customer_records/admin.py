from django.contrib import admin
from customer_records.models import CustomerSubscriptionRecord


class CustomerSubscriptionRecordAdmin(admin.ModelAdmin):
    model = CustomerSubscriptionRecord
    list_display = ("name", "email", "subscription")
    list_filter = ("subscription",)
    search_fields = ("name", "email", "subscription")


admin.site.register(CustomerSubscriptionRecord, CustomerSubscriptionRecordAdmin)
