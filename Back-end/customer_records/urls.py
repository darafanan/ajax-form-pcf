from django.conf.urls import url, include
from rest_framework import routers

from customer_records.views import SubscriptionRecordsViewSet, subscription_choices

router = routers.DefaultRouter()
router.register("subscriptions", SubscriptionRecordsViewSet, basename="ajaxform")


urlpatterns = [
    url("", include(router.urls)),
    url(r"^subscription_choices/", subscription_choices),
]
