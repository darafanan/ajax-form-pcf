from django.apps import AppConfig


class CustomerRecordsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'customer_records'
