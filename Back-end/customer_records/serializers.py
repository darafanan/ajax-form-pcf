from rest_framework import serializers
from customer_records.models import CustomerSubscriptionRecord


class CustomerSubscriptionRecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerSubscriptionRecord
        fields = ("id", "name", "email", "subscription")
