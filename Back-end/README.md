## Project setup for Ubuntu 18 with Python 3
```
sudo apt update
sudo apt install python3-pip python3-dev
```

### Install requirements
```
pip install -r ./requirements.txt --ignore-installed --force-reinstall --upgrade --no-cache-dir
```


### Migrate the Database
```
python manage.py migrate
```


### Run Server
```
python manage.py runserver
```