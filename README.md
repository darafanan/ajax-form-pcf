# ajax-form-pcf

A simple Django application with the following features:  

An AJAX form which creates a record with the following fields:  
* Customer Name  
* Email Address (must be a valid email address format)  
* Subscription Type (with the options: Free, Plus, Pro)  
* Display  

Back-end folder was created using django (https://www.djangoproject.com/)  
Front-end folder was created using vue.js (https://vuejs.org/)  
